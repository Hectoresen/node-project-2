const express = require('express');
const PORT = 3000;
const server = express();
const mainRoutes = require('./routes/main.routes');
const teamsRoutes = require('./routes/teams.routes');
const playerRoutes = require('./routes/players.routes');

//Funciones propias de express que transforman la información
//enviada como JSON al servidor
//Así puedo obtenerla en req.body
server.use(express.json());
//Si la petición (POST, PUT..) viene en formato urlencoded
server.use(express.urlencoded({ extended: false }));
//Fin funciones propias de express

//Error handler
/* server.use((err, req, res) =>{
    const status = err.status || 500;
    const message = err.message || 'Unexpected error';
    return res.status(status).json(message);
}) */

const {connect} = require('./config/db');
connect();


server.use('/', mainRoutes);
server.use('/teams', teamsRoutes);
server.use('/players', playerRoutes);

server.listen(PORT, () =>{
    console.log(`Server running in http://localhost:${PORT}`);
});