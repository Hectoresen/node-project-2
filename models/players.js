const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const playersSchema = new Schema({
    name: {type: String, required: true},
    age: {type: Number, required: true},
    //Hace referencia al modelo de equipos (teams.js)
    team: [{ type: mongoose.Types.ObjectId, ref: 'team' }]
},
{timestamps: true}
);

const playersModel = mongoose.model('players', playersSchema);

module.exports = playersModel;