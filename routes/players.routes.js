const express = require('express');
const playersModel = require('../models/players');
const TeamsModel = require('../models/teams');
const router = express.Router();


//Get by player name method Postman
router.get('/search/:player', async(req, res, next) =>{
    try{
        const searchPlayer = req.params.player;
        const players = await playersModel.find({name: searchPlayer});
        return res.status(200).json(players);
    }catch(err){
        return res.status(404).json(err);
    }
});

//Delete method Postman
router.delete('/delete/:id', async(req, res, next) =>{
    try{
        const {id} = req.params;
        await playersModel.findByIdAndDelete(id);
        return res.status(200).json('Player deleted!');
    }catch(err){
        return next(err);
    }
});

//Post method Postman
router.post('/create', async (req, res, next) =>{
    try{
        const {name, age} = req.body;
        const newPlayer = new playersModel({
            name,
            age,
            team: []
        });
        const createdPlayer = await newPlayer.save();
        return res.status(201).json(createdPlayer);
    }catch(err){
        next(err);
    }
});

//Add team ID to array team in player database
router.put('/add-player', async(req, res, next) =>{
    try{
        const {teamId, playerId} = req.body;
        const updatedTeam = await playersModel.findByIdAndUpdate(
            playerId,
            {$push: {team: teamId}},
            {new: true}
        );
        return res.status(200).json(updatedTeam);
    }catch(err){
        return next(err)
    }
});

router.get('/info', async(req,res,next) =>{
    try{
        //Populate hace referencia al array "team" vacío de playersModel
        const players = await playersModel.find().populate('team');
        return res.status(200).json(players);
    }catch(err){
        return next(err);
    }
});



module.exports = router;
