const express = require('express');
const TeamsModel = require('../models/teams');

const router = express.Router();

router.get('/allteams', async (req, res) =>{
    try {
        const teams = await TeamsModel.find();
        return res.status(200).json(teams);
    } catch(err){
        return res.status(404).json(err);
    }
});
//Post method Postman
router.post('/create', async (req, res, next) =>{
    try {
        const { team, leaguePosition, totalGoals} = req.body;
        //Esto es igual que team = req.body.team / leaguePosition = req.body.leaguePosition, etc
        const newTeam = new TeamsModel({
            team,
            leaguePosition,
            totalGoals,
        });
        const createdTeam = await newTeam.save();
        return res.status(201).json(createdTeam);
    } catch(err){
        next(err);
    }
});
//Put method Postman
router.put('/edit/:id', async (req, res, next) =>{
    try {
        const {id} = req.params;
        const teamModify = new TeamsModel(req.body);
        teamModify._id = id;
        const teamUpdated = await TeamsModel.findByIdAndUpdate(id, teamModify);
        return res.status(200).json(teamUpdated);
    } catch(err){
        return next(err);
    }
});
//Delete method Postman
router.delete('/delete/:id', async (req, res, next) =>{
    try {
        const {id} = req.params;
        await TeamsModel.findByIdAndDelete(id);
        return res.status(200).json('Team deleted!');
    } catch(err){
        return next(err);
    }
});

//Search by id
router.get('/allteams/:id', async(req, res) =>{
    const id = req.params.id;
    const teams = await TeamsModel.findById(id);
    if(teams){
        return res.status(200).json(teams);
    }else{
        return res.status(404).json('We have not found the requested content');
    }
});
//Team search
router.get('/allteams/name/:team', async (req, res) =>{
    try{
        const name = req.params.team;
        const teams = await TeamsModel.find({
            team: {$regex: new RegExp("^" + name.toLowerCase(), "i"),
        },
        });
        return res.status(200).json(teams);
    }catch(err){
        return res.status(404).json('We have not found the requested content');
    }
});
//Search by position league
router.get('/allteams/name/team/:leagueposition', async (req, res) =>{
    try {
        const leaguePositionTeam = req.params.leagueposition;
        const teams = await TeamsModel.find({leaguePosition: leaguePositionTeam});
        return res.status(200).json(teams);
    } catch{
        return res.status(404).json('We have not found the requested content');
    }
})
//Search same goals or greater
router.get('/allteams/name/team/goals/:totalgoals', async (req, res) =>{
    try {
        const teamGoals = req.params.totalgoals;
        const teams = await TeamsModel.find({totalGoals:{$gte:teamGoals}})
        return res.status(200).json(teams);
    } catch {
        return res.status(404).json('We have not found the requested content')
    }
})

module.exports = router;