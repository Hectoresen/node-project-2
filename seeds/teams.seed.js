const mongoose = require('mongoose');
const TeamsModel = require('../models/teams');
const {DB_URL, CONFIG_DB} = require('../config/db');

const teamList = [
    {
        team: 'Alavés',
        leaguePosition: 4,
        totalGoals: 32
    },
    {
        team: 'Barcelona',
        leaguePosition: 18,
        totalGoals: 12
    },
    {
        team: 'Osasuna',
        leaguePosition: 15,
        totalGoals: 19
    },
    {
        team: 'Betis',
        leaguePosition: 12,
        totalGoals: 14
    },
    {
        team: 'Valencia',
        leaguePosition: 3,
        totalGoals: 40
    },
    {
        team: 'Real Sociedad',
        leaguePosition: 2,
        totalGoals: 42
    },
    {
        team: 'Real Madrid',
        leaguePosition: 1,
        totalGoals: 9000
    },
];

mongoose.connect(DB_URL, CONFIG_DB)
    .then(async() =>{
        console.log('Running seed');
        const allTeams = await TeamsModel.find();
        console.log(allTeams);
        (allCharacters.length) ? await TeamsModel.collection.drop() : '';
    })
    .catch(err => console.log('Collection removed successfully'))
    .then(async () =>{
        await TeamsModel.insertMany(teamList);
        console.log('Teams added to database');
    })
    .catch(err => console.log(`Error adding teams to database`))
    //Finally always runs
    .finally(() => mongoose.disconnect)